from django.contrib.syndication.views import Feed
from django.contrib.sites.models import Site
from django.utils.feedgenerator import Atom1Feed
from ervin.models import OnlineEdition, Page, PhysicalEdition
import ervin.conf


class NewsFeed(Feed):
    feed_guid = ervin.conf.NEWS_FEED_ID
    feed_type = Atom1Feed
    link = "/feeds/news"
    title = ervin.conf.NEWS_FEED_TITLE
    authors = [{'name':  ervin.conf.FEED_AUTHOR,
                'email': ervin.conf.FEED_EMAIL}]

    def items(self):
        return Page.objects.filter(news=True).order_by('-published')[0:10]

    def item_id(self, item):
        return 'http://%s%s' % (Site.objects.get_current().domain,
                                item.get_absolute_url())

    def item_title(self, item):
        return item.title

    def item_updateddate(self, item):
        return item.published

    def item_pubdate(self, item):
        return item.published


class EditionFeed(Feed):
    def item_title(self, ed):
        return ed.title

    def item_updateddate(self, ed):
        return ed.date

    def item_pubdate(self, ed):
        return ed.date

    def item_link(self, ed):
        return 'http://%s%s' % (Site.objects.get_current().domain,
                                ed.work.get_absolute_url())

    def item_authors(self, ed):
        return [{'name': unicode(a)} for a in ed.authors.all()]


class RecentDocumentsFeed(EditionFeed):
    feed_type = Atom1Feed
    feed_guid = ervin.conf.RECENT_DOCUMENTS_FEED_ID
    link = "/feeds/recent-documents"
    title = ervin.conf.RECENT_DOCUMENTS_FEED_TITLE
    authors = [{'name':  ervin.conf.FEED_AUTHOR,
                'email': ervin.conf.FEED_EMAIL}]

    def items(self):
        return OnlineEdition.with_content.order_by('-date')[0:20]


class RecentPublicationsFeed(EditionFeed):
    feed_type = Atom1Feed
    feed_uiid = ervin.conf.RECENT_PUBLICATIONS_FEED_ID
    title = ervin.conf.RECENT_PUBLICATIONS_FEED_TITLE
    authors = [{'name':  ervin.conf.FEED_AUTHOR,
                'email': ervin.conf.FEED_EMAIL}]

    def items(self):
        return PhysicalEdition.objects.order_by('-date_sort')[0:10]

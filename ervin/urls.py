#Copyright (C) 2007-2009, Erik Hetzner

#This file is part of Ervin.  Ervin is free software: you can
#redistribute it and/or modify it under the terms of the GNU General
#Public License as published by the Free Software Foundation, either
#version 3 of the License, or (at your option) any later version.

#Ervin is distributed in the hope that it will be useful, but WITHOUT
#ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#for more details.

#You should have received a copy of the GNU General Public License
#along with Ervin.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url, include

import ervin.models, ervin.feeds, django.contrib.syndication.views
from django.views.generic.base import RedirectView
from ervin.views import generic, work, unapi, page, ol, main

urlpatterns = [
    url(r'^concepts$', generic.list_view, {'class' : ervin.models.Concept,
                                          'columns' : 4 }),
    url(r'^events$', generic.list_view, {'class' : ervin.models.Event,
                                        'columns' : 4 }),
    url(r'^places$', generic.list_view, {'class' : ervin.models.Place,
                                         'columns' : 4 }),
    url(r'^objects$', generic.list_view, {'class' : ervin.models.FrbrObject }),
    url(r'^persons$', generic.list_view, {'class' : ervin.models.Person,
                                         'columns' : 4 }),
    url(r'^people$', generic.list_view, {'class' : ervin.models.Person,
                                         'columns' : 4 }),
    url(r'^organizations$', generic.list_view, {'class' : ervin.models.Organization,
                                               'columns' : 4 }),
    url(r'^works$', generic.list_view, {'class' : ervin.models.Work }),
    url(r'^documents$', work.online_works),
    url(r'^physicaleditions$', generic.list_view, {'class' : ervin.models.PhysicalEdition }),
    url(r'^unapi$', unapi.unapi),
    url(r'^doc/(?P<id>.*)$', page.by_id),
    url(r'^(?P<olkey>a/[A-Za-z0-9-]+)$', ol.author),
    url(r'^(?P<olkey>b/[A-Za-z0-9-]+)$', ol.edition),
    url(r'^feeds/recent-documents$', ervin.feeds.RecentDocumentsFeed()),
    url(r'^feeds/news$', ervin.feeds.NewsFeed()),
    url(r'^(?P<noid>[a-z0-9-]{6})$', generic.by_noid),
    url(r'^redactor/', include('redactor.urls')),
    #redirect noids with a trailing slash
    url(r'^(?P<noid>[a-z0-9-]{6})/$',
     RedirectView.as_view(url='/%(noid)s')),
    url(r'^$', main.home)
]

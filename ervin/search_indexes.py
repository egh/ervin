import datetime
from haystack import indexes
from ervin.models import OnlineEdition, PhysicalEdition

class OnlineEditionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    subjects = indexes.MultiValueField(faceted=True)
    authors = indexes.MultiValueField(faceted=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return OnlineEdition

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_subjects(self, obj):
        return [ unicode(s) for s in obj.work.all_subjects.all() ]

    def prepare_authors(self, obj):
        return [ unicode(a) for a in obj.authors.all() ]

class PhysicalEditionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    subjects = indexes.MultiValueField(faceted=True)
    authors = indexes.MultiValueField(faceted=True)
    title = indexes.CharField(model_attr='title')

    def get_model(self):
        return PhysicalEdition

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_subjects(self, obj):
        return [ unicode(s) for s in obj.work.all_subjects.all() ]

    def prepare_authors(self, obj):
        return [ unicode(a) for a in obj.authors.all() ]
